package org.odk.collect.android.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONObject;
import org.odk.collect.android.activities.MainMenuActivity;
import org.odk.collect.android.application.Collect;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Borey on 3/17/15.
 */
public class WellMapDBHelper extends ODKSQLiteOpenHelper{

    public WellMapDBHelper(String dbPath){
        super(dbPath,"well.db",null,1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = "create table if not exists well_assessment_core(id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "ADMNSTRTIVE_INFRMTON_VILLAGE_CODE TEXT,INFO_ABOUT_WELL_WELL_OWNER_NAME TEXT, " +
                "INFO_ABOUT_WELL_TYPEOFWELL TEXT, INFO_ABOUT_WELL_WELL_ID TEXT, " +
                "INFO_ABOUT_WELL_WELL_LOCATION_LAT TEXT,INFO_ABOUT_WELL_WELL_LOCATION_LNG TEXT, " +
                "_LAST_UPDATE_DATE TEXT,INFO_ABOUT_WELL_WELLOWNER_PHONE TEXT, INFO_ABOUT_WELL_DATE_WELL TEXT," +
                "MONTRNG_NFRMTN_DTIL_HIDDEN)";

        db.execSQL(sql);
    }
    public int dbSyncCount(){
        int count = 0;
        String selectQuery = "SELECT  * FROM well_assessment_core";
        SQLiteDatabase database = this.getReadableDatabase();
        Cursor cursor = database.rawQuery(selectQuery, null);
        count = cursor.getCount();
        database.close();
        return count;
    }

    public void insertRow(String village_code, String owner, String well_type, String well_id,
                String lat, String lng, String last_date, String owner_phone, String well_date, String syncStatus){

        if(village_code.length() < 8){
            village_code = '0'+village_code;
        }
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String insertQuery = "insert into well_assessment_core(ADMNSTRTIVE_INFRMTON_VILLAGE_CODE," +
                "INFO_ABOUT_WELL_WELL_OWNER_NAME, INFO_ABOUT_WELL_TYPEOFWELL, " +
                "INFO_ABOUT_WELL_WELL_ID,INFO_ABOUT_WELL_WELL_LOCATION_LAT," +
                "INFO_ABOUT_WELL_WELL_LOCATION_LNG,_LAST_UPDATE_DATE,INFO_ABOUT_WELL_WELLOWNER_PHONE," +
                "INFO_ABOUT_WELL_DATE_WELL,MONTRNG_NFRMTN_DTIL_HIDDEN) values("+
                "'"+village_code+"'"+","+"'"+owner+"'"+","+"'"+well_type+"'"+","+"'"+
                well_id+"'"+","+"'"+lat+"'"+","+"'"+lng+"'"+","+"'"+last_date+"'"+","+"'"+
                owner_phone+"'"+","+"'"+well_date+"'"+","+"'"+syncStatus+"'"+")";
        sqLiteDatabase.execSQL(insertQuery);
    }


    public String selectMaxDate(){

        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
        String selectMax = "select _LAST_UPDATE_Date from well_assessment_core order by id desc limit 1";
        Cursor cursor = sqLiteDatabase.rawQuery(selectMax,null);
        String maxDate = (cursor.moveToFirst() ? cursor.getString(0) : "2013-01-01 00:00:00");
        return maxDate;

    }

    public String getRows(){
        ArrayList<HashMap<String,String>> arrayListMap = new ArrayList<HashMap<String, String>>();
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String sql = "select ADMNSTRTIVE_INFRMTON_VILLAGE_CODE, INFO_ABOUT_WELL_WELL_OWNER_NAME," +
                "INFO_ABOUT_WELL_WELL_ID, INFO_ABOUT_WELL_WELL_LOCATION_LAT, INFO_ABOUT_WELL_WELL_LOCATION_LNG," +
                "INFO_ABOUT_WELL_WELLOWNER_PHONE, INFO_ABOUT_WELL_DATE_WELL," +
                "MONTRNG_NFRMTN_DTIL_HIDDEN from well_assessment_core " +
                "where MONTRNG_NFRMTN_DTIL_HIDDEN='no' and INFO_ABOUT_WELL_WELL_ID = 'null'";
        Cursor cursor = sqLiteDatabase.rawQuery(sql,null);
        if(cursor.moveToFirst()){
            do {
                HashMap<String, String> hashMap = new HashMap<String, String>();
                hashMap.put("ADMNSTRTIVE_INFRMTON_VILLAGE_CODE",cursor.getString(0));
                hashMap.put("INFO_ABOUT_WELL_WELL_OWNER_NAME",cursor.getString(1));
                //No need well_id to sync cos it has 'null' value
                //hashMap.put("INFO_ABOUT_WELL_WELL_ID",cursor.getString(2));
                hashMap.put("INFO_ABOUT_WELL_WELL_LOCATION_LAT",cursor.getString(3));
                hashMap.put("INFO_ABOUT_WELL_WELL_LOCATION_LNG",cursor.getString(4));
                hashMap.put("INFO_ABOUT_WELL_WELLOWNER_PHONE",cursor.getString(5));
                hashMap.put("INFO_ABOUT_WELL_DATE_WELL",cursor.getString(6));
                hashMap.put("MONTRNG_NFRMTN_DTIL_HIDDEN",cursor.getString(7));
                arrayListMap.add(hashMap);
            }while (cursor.moveToNext());

        }
        Gson gson = new GsonBuilder().create();
        return gson.toJson(arrayListMap);
    }
    public void updateSync(String villageCode){
        SQLiteDatabase db = this.getWritableDatabase();
        String sql = "update well_assessment_core set MONTRNG_NFRMTN_DTIL_HIDDEN='yes' where " +
                "ADMNSTRTIVE_INFRMTON_VILLAGE_CODE='"+villageCode+"'";
        db.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table if exists well_assessment_core");
        this.onCreate(db);
    }
}
